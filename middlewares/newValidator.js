const bodyValidator = (schema) => async (req, res, next) => {
  try {
    await schema.validateAsync(req.body);
    next();
  } catch (error) {
    console.log(error);
    error.message = `validation failed ${error.message}`;
    error.status = 400;
    next(error);
  }
};

module.exports = {
  bodyValidator,
};
