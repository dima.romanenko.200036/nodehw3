const {
  bodyValidator,
} = require('./newValidator');
const {
  register,
  login,
  forgotPassword,
  updatePassword,
  addTruck,
  updateTruck,
  postLoad,
} = require('./joi');

module.exports = {
  registerValidator: bodyValidator(register),
  loginValidator: bodyValidator(login),
  forgotPasswordValidator: bodyValidator(forgotPassword),
  updatePasswordValidator: bodyValidator(updatePassword),
  addTruckValidator: bodyValidator(addTruck),
  updateTruckValidator: bodyValidator(updateTruck),
  addLoadValidator: bodyValidator(postLoad),
};
