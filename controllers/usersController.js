const {
  getUserById,
  updateUserPassword,
  deleteUserById,
} = require('../services/usersService');

const getUser = async (req, res) => {
  // console.log('id from req: ', req.user._id);
  const user = await getUserById(req.user._id);
  res.status(200).json(user);
};

const deleteUser = async (req, res) => {
  await deleteUserById(req.user._id);
  res.status(200).json({
    message: 'Profile deleted successfully',
  });
};

const updatePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  await updateUserPassword({_id: req.user._id, oldPassword, newPassword});
  res.status(200).json({
    message: 'Password changed successfully',
  });
};


module.exports = {getUser, deleteUser, updatePassword};
