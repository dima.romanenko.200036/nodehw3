require('dotenv').config();
require('./config/dbConfig');
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const customErrorHandler = require('./tools/customErrorHandler');
const PORT = process.env.PORT;
const route = require('./routes/mainRoute');
const app = express();
app.use(morgan('combined'));
app.use(express.json());
app.use(cors());
app.use(route);
app.use(customErrorHandler);

app.use((req, res) => {
  res.status(404).json({ message: 'Not found' });
});

app.use((err, req, res, next) => {
  res.status(500).json({ message: err.message });
});

app.listen(PORT, () => {
  console.log(`Server running on port: ${PORT}`);
});
