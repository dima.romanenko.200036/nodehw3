const express = require('express');
const errorWrapper = require('../tools/errorAwaiter');
const {
  getUser,
  deleteUser,
  updatePassword,
} = require('../controllers/usersController');
const {updatePasswordValidator} = require('../middlewares/bodyValidator');
const router = new express.Router();

router.get('/', errorWrapper(getUser));
router.delete('/', errorWrapper(deleteUser));
router.patch(
    '/password',
    updatePasswordValidator,
    errorWrapper(updatePassword),
);

module.exports = router;
