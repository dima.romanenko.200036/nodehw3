const express = require('express');

const errorWrapper = require('../tools/errorAwaiter');
const {
  addTruck,
  assignTruck,
  getTrucks,
  getTruck,
  updateTruck,
  deleteTruck,
} = require('../controllers/trucksController');
const {
  addTruckValidator,
  updateTruckValidator,
} = require('../middlewares/bodyValidator');

const router = new express.Router();

router.get('/', errorWrapper(getTrucks));
router.post('/', addTruckValidator, errorWrapper(addTruck));
router.get('/:id', errorWrapper(getTruck));
router.put('/:id', updateTruckValidator, errorWrapper(updateTruck));
router.delete('/:id', errorWrapper(deleteTruck));
router.post('/:id/assign', errorWrapper(assignTruck));

module.exports = router;
