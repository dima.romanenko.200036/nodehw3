const errorWrapper = (operation) => async (req, res, next) => {
  try {
    await operation(req, res, next);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

module.exports = errorWrapper;
