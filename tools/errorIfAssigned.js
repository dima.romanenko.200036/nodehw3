const throwIfAssigned = ({userId, assignedTo}) => {
  if (userId === String(assignedTo)) {
    const er = new Error('Action is not allowed for trucks assigned to driver');
    er.status = 400;
    throw er;
  }
};

module.exports = throwIfAssigned;
